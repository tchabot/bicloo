import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import Map from './components/Map';

const styles = StyleSheet.create({
  title: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 40,
    fontWeight: 'bold',
  },
  map: {
    flex: 4,
  },
});

export default class App extends Component {
  render() {
    return (
      <>
        <View style={styles.title}>
          <Text fontWeight="bold">Bicloo NANTES</Text>
        </View>
        <View style={styles.map}>
          <Map />
        </View>
      </>
    );
  }
}
