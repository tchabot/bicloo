import React from 'react';
import {useGeolocation} from 'beautiful-react-hooks';

export default function PositionReporter() {
  const [geoState, {onChange}] = useGeolocation({
    enableHighAccuracy: true,
    timeout: 0xffffffff,
    maximumAge: 0,
  });

  onChange(() => {
    console.log('Position changed...');
  });

  return (
    <div>
      The current position is:
      {geoState.isRetrieving && <p>Retrieving position...</p>}
      {geoState.isSupported &&
        geoState.position && [
          <p key={0}>Lat: {geoState.position.coords.latitude}</p>,
          <p key={1}>Lng: {geoState.position.coords.longitude}</p>,
        ]}
    </div>
  );
}
