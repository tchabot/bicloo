import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';

import places from '../places.json';

const img = require('../images/mk_place.png');

export default class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      places: places,
      currentLatitude: 47.21345,
      currentLongitude: -1.55527,
      distance: 0,
    };
  }

  componentDidMount() {
    Geolocation.getCurrentPosition(result => {
      this.setState({
        currentLatitude: parseFloat(
          Number.parseFloat(result.coords.latitude).toFixed(6),
        ),
        currentLongitude: parseFloat(
          Number.parseFloat(result.coords.longitude).toFixed(6),
        ),
      });
    });
  }

  getDistanceBetween(mk1lat, mk1long, mk2lat, mk2long) {
    var R = 6372.795; // Radius of the Earth in miles
    var rlat1 = mk1lat * (Math.PI / 180); // Convert degrees to radians
    var rlat2 = mk2lat * (Math.PI / 180); // Convert degrees to radians
    var difflat = rlat2 - rlat1; // Radian difference (latitudes)
    var difflon = (mk2long - mk1long) * (Math.PI / 180); // Radian difference (longitudes)

    var d =
      2 *
      R *
      Math.asin(
        Math.sqrt(
          Math.sin(difflat / 2) * Math.sin(difflat / 2) +
            Math.cos(rlat1) *
              Math.cos(rlat2) *
              Math.sin(difflon / 2) *
              Math.sin(difflon / 2),
        ),
      );
    return d;
  }

  handleRegionChange() {
  }
  

  handleMapReady(params) {
    console.log('map ready');
  }

  handleOnLayout() {
    console.log('On layout');
    let nearPlaces = this.state.places.filter(place => {
      let distance = this.getDistanceBetween(
        this.state.currentLatitude,
        this.state.currentLongitude,
        place.latitude,
        place.longitude,
      );
      if (distance <= 0.5) {
        return place;
      }
    });
    this.setState({
      places: nearPlaces,
    });
  }

  render() {
    return (
      <View style={this.styles.container}>
        <Text>Places</Text>
        <MapView
          provider={PROVIDER_GOOGLE}
          style={this.styles.map}
          onMapReady={this.handleMapReady.bind(this)}
          onLayout={this.handleOnLayout.bind(this)}
          onRegionChange={this.handleRegionChange.bind(this)}
          initialRegion={{
            latitude: this.state.currentLatitude,
            longitude: this.state.currentLongitude,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}>
          {this.state.places.map(marker => (
            <Marker
              key={marker.key}
              coordinate={{
                latitude: marker.latitude,
                longitude: marker.longitude,
              }}
              title={'Vélos disponibles : ' + marker.nb}
              image={img}
            />
          ))}
          <Marker
            key={42}
            coordinate={{
              latitude: this.state.currentLatitude,
              longitude: this.state.currentLongitude,
            }}
          />
        </MapView>
      </View>
    );
  }

  styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
      height: 400,
      width: 400,
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    map: {
      ...StyleSheet.absoluteFillObject,
    },
  });
}
