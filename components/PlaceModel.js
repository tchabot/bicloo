const EARTH_RADIUS_KMS = 6372.795;

const degreeToRadian = coord => (coord * Math.PI) / 180;

export default class Place {
  constructor(lat, lng) {
    this.coordinate = {
      latitude: lat,
      longitude: lng,
    };
  }

  constructor(place) {
    this.key = place.key;
    this.capacity = place.nb; // NB_PLACE
  }

  compare(other) {
    return this.distance - other.distance;
  }

  setDistanceFrom(lat, lng) {
    const selfLatRad = degreeToRadian(this.coordinate.latitude);
    const latRad = degreeToRadian(lat);
    const x =
      (degreeToRadian(this.coordinate.longitude) - degreeToRadian(lng)) *
      Math.cos((latRad + selfLatRad) / 2);
    const y = selfLatRad - latRad;
    this.distance = Math.sqrt(x * x + y * y) * EARTH_RADIUS_KMS;
  }
}
